#!/usr/bin/env bash

# This script is licensed under the so-called '3-clause BSD License',
# also known as the 'New BSD License'. The license text reads as
# follows:
#
# Copyright 2018-2019 Thomas Fischer <fischer@unix-ag.uni-kl.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

APPLICATION="$(basename "$0")"
APPLICATION="${APPLICATION/run-/}"
APPLICATION="${APPLICATION/.sh/}"
GIT_URL="${1:-https://invent.kde.org/office/kbibtex.git}" # Anonymously cloning KBibTeX's repository
GIT_URL="${1:-https://anongit.kde.org/kbibtex.git}" # Anonymously cloning KBibTeX's repository
GIT_URL="$(sed -r 's!(anongit|cgit).kde.org!invent.kde.org!g;s!/clones/kbibtex/thomasfischer/kbibtex.git/?!/thomasfischer/kbibtex.git!;s!org([:/])kbibtex.git/?!org\1office/kbibtex.git!g' <<<"${GIT_URL}")" # Fix mistake of using wrong domain or old Git server
GIT_BRANCH="${2:-master}" # 'master' is default branch
GIT_DIRECTORY="/tmp/${USER}-${APPLICATION}-git-$(md5sum <<<"${GIT_URL}" | cut -f 1 -d ' ')"
INSTALL_PREFIX="/tmp/${USER}-${APPLICATION}-usr"
BUILD_DIRECTORY="/tmp/${USER}-${APPLICATION}-build-$(md5sum <<<"${GIT_URL}${GIT_BRANCH}" | cut -f 1 -d ' ')"
XDG_DIRECTORY="/tmp/${USER}-${APPLICATION}-xdg-$(md5sum <<<"${GIT_URL}${GIT_BRANCH}" | cut -f 1 -d ' ')"

echo "Application:                  ${APPLICATION}"
echo "Expected application binary:  ${INSTALL_PREFIX}/bin/${APPLICATION}"
echo "Git URL:                      ${GIT_URL}"
echo "Git branch or tag:            ${GIT_BRANCH}"
echo "Installation prefix:          ${INSTALL_PREFIX}"
echo "Build directory:              ${BUILD_DIRECTORY}"
echo "Configuration directory:      ${XDG_DIRECTORY}"

# Checking the existance of various necessary binaries
for necessarybinary in git cmake make c++ pkg-config okular kate kbuildsycoca5 valgrind ; do
	which ${necessarybinary} 2>/dev/null >&2 || { echo "Could not locate necessary binary '${necessarybinary}'" >&2 ; exit 1 ; }
done
# Basic checks on various directories in /tmp
for directory in "${GIT_DIRECTORY}" "${INSTALL_PREFIX}" "${BUILD_DIRECTORY}" "${XDG_DIRECTORY}" ; do
	[[ -e "${directory}" && ! -d "${directory}" ]] && { echo "Filesystem entry '${directory}' exists but is not a directory" >&2 ; exit 1 ; }
	[[ -e "${directory}" && ! -O "${directory}" ]] && { echo "Directory '${directory}' exists but is not owned by effective user" >&2 ; exit 1 ; }
done

# Retrieve source code via Git: either fresh checkout or just pulling updates
if [[ ! -d "${GIT_DIRECTORY}" || ! -d "${GIT_DIRECTORY}/.git" ]] ; then
	# No valid checkout found, so clone from repository
	rm -rf "${GIT_DIRECTORY}" || exit 1
	( cd $(dirname "${GIT_DIRECTORY}") && git clone "${GIT_URL}" $(basename "${GIT_DIRECTORY}") ) || exit 1
	# Switch to right branch
	( cd "${GIT_DIRECTORY}" && git checkout "${GIT_BRANCH}" ) || exit 1
else
	# Switch to right branch
	( cd "${GIT_DIRECTORY}" && git checkout "${GIT_BRANCH}" ) || exit 1
	# ... and pull the latest updates (fast-forward only)
	( cd "${GIT_DIRECTORY}" && git pull --ff-only ) || exit 1
fi
# Remove read-write-executable permissions for everyone else from this directory tree
chmod -R go-rwx "${GIT_DIRECTORY}" || exit 1

# Create build directory if it does not yet exist
mkdir -p "${BUILD_DIRECTORY}" || exit 1
# Remove read-write-executable permissions for everyone else from this directory tree
chmod -R go-rwx "${BUILD_DIRECTORY}" || exit 1

# Reduce CPU and I/O priority
renice -n 15 $$ >/dev/null 2>&1
ionice -c 2 -n 5 -p $$ >/dev/null 2>&1

# Run 'cmake' to configure the build process
( cd "${BUILD_DIRECTORY}" && cmake -DCMAKE_BUILD_TYPE=debug -DCMAKE_INSTALL_PREFIX:PATH="${INSTALL_PREFIX}" "${GIT_DIRECTORY}" ) || exit 1

# Run 'make' to build/compile the source code
( cd "${BUILD_DIRECTORY}" && make -j $(nproc) ) || exit 1

# Always start from a fresh installation directory
rm -rf "${INSTALL_PREFIX}"
# Create a 'lib' directory inside installation directory
mkdir -p "${INSTALL_PREFIX}/lib" || exit 1
# ... and have 'lib64' link to it
( cd ${INSTALL_PREFIX} && ln -s lib lib64 ) || exit 1
# Remove read-write-executable permissions for everyone else from this directory tree
chmod -R go-rwx "${INSTALL_PREFIX}" || exit 1

# Set various environment variables so that this temporary directory is recognized as a valid installation destination
export LD_LIBRARY_PATH=${INSTALL_PREFIX}/lib
export XDG_CACHE_HOME=${XDG_DIRECTORY}/cache
export XDG_CONFIG_HOME=${XDG_DIRECTORY}/config
export XDG_DATA_DIRS=${INSTALL_PREFIX}/share:/usr/share:/usr/local/share
export XDG_DATA_HOME=${XDG_DIRECTORY}/data
export PATH=${INSTALL_PREFIX}/bin:${PATH}
KDEDIR="$(dirname $(which kate) | sed -e 's!/bin$!!g')"
export KDEDIRS=${INSTALL_PREFIX}:${KDEDIR}

# Request very verbose debug output for anything from KBibTeX
export QT_LOGGING_RULES="kbibtex.*.debug=true"
# How to format log output
export QT_MESSAGE_PATTERN="[%{type}] %{appname} (%{file}:%{line}) - %{message}"

# Install files in installation prefix
( cd "${BUILD_DIRECTORY}" && make -j $(nproc) install ) || exit 1

# Assemble QT_PLUGIN_PATH by locating all relevant directories
for stem in lib lib64 plugins ; do
	while read d ; do test -d "${d}" && QT_PLUGIN_PATH="${QT_PLUGIN_PATH}:${d}" ; done <<<$(find "${INSTALL_PREFIX}" -maxdepth 2 -type d -name ${stem} )
	while read d ; do test -d "${d}" && QT_PLUGIN_PATH="${QT_PLUGIN_PATH}:${d}" ; done <<<$(find "${KDEDIR}" -maxdepth 2 -type d -name ${stem} 2>/dev/null)
done
export QT_PLUGIN_PATH="$(find "${INSTALL_PREFIX}" -name kbibtexpart.so -executable -print0 | xargs -0 dirname | head -n 1)${QT_PLUGIN_PATH}"

# Rebuild the KService desktop file system configuration cache -- necessary to locate KBibTeX's KPart
( cd ${INSTALL_PREFIX} && kbuildsycoca5 ) || exit 1

# Prepare command line arguments for application
# !!! In order to pass arguments to the application, both optional arguments for this
# !!!  this script must be given first: Git repository URL and branch name
# !!! Application's command line arguments are the third and later arguments to this script
if (( $# >= 2 )) ; then shift 2 ; else shift $# ; fi

# Launch actual KBibTeX binary
( cd ${HOME} && ${INSTALL_PREFIX}/bin/${APPLICATION} "${@}" ) || exit 1
# As an alternative to a regular application launch, use valgrind as exemplified below:
## ( cd ${HOME} && valgrind --error-limit=no --smc-check=all-non-file --track-origins=yes --log-file=/tmp/${USER}-${APPLICATION}-valgrind-$(date '+%Y%m%d-%H%M%S').txt --show-reachable=yes --num-callers=24 -v --leak-check=full ${INSTALL_PREFIX}/bin/${APPLICATION} "${@}" ) || exit 1
