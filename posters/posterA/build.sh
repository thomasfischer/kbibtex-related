#!/usr/bin/env bash

TEMPDIR=$(mktemp -d)

lualatex -halt-on-error --output-directory=${TEMPDIR} kbibtex-poster.tex && lualatex -halt-on-error --output-directory=${TEMPDIR} kbibtex-poster.tex && cp -p ${TEMPDIR}/kbibtex-poster.pdf .

rm -rf ${TEMPDIR}
