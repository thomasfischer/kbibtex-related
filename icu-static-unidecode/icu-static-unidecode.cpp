/*
 * This software is released under the so-called '3-Clause BSD License'
 * (BSD-3-Clause)
 *
 * Copyright 2019 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/// How to compile this code on Linux or similar Unix flavors:
///   c++ $(pkg-config --cflags icu-uc icu-i18n) -Wall -std=c++11 -c -o icu-static-unidecode.o icu-static-unidecode.cpp
///   c++ $(pkg-config --libs icu-uc icu-i18n) -o icu-static-unidecode icu-static-unidecode.o
/// or use Makefile

#include <iostream>
#include <map>
#include <vector>
#include <string>

#include <unicode/translit.h>

struct first_len_then_lex : std::binary_function<std::string, std::string, bool>
{
    inline bool operator()(const std::string &lhs, const std::string &rhs) {
        /// Sort by string length first, second by string comparison (lexicographically?)
        if (lhs.length() != rhs.length())
            return lhs.length() > rhs.length();
        else // lhs.length() == rhs.length()
            return lhs < rhs;
    }
};

inline bool codePointToUtf8(uint32_t codepoint, char *utf8buffer /* must be at least 5 characters long! */)
{
    if (codepoint <= 0x007f) {
        utf8buffer[0] = static_cast<char>(codepoint & 0x7f);
        utf8buffer[1] = '\0';
        return true;
    } else if (codepoint <= 0x07ff) {
        utf8buffer[0] = static_cast<char>((codepoint >> 6) | 0xc0);
        utf8buffer[1] = static_cast<char>((codepoint & 0x3f) | 0x80);
        utf8buffer[2] = '\0';
        return true;
    } else if (codepoint <= 0xffff) {
        utf8buffer[0] = static_cast<char>((codepoint >> 12) | 0xe0);
        utf8buffer[1] = static_cast<char>(((codepoint >> 6) & 0x3f) | 0x80);
        utf8buffer[2] = static_cast<char>((codepoint & 0x3f) | 0x80);
        utf8buffer[3] = '\0';
        return true;
    } else if (codepoint <= 0x10FFFF) {
        utf8buffer[0] = static_cast<char>((codepoint >> 18) | 0xf0);
        utf8buffer[1] = static_cast<char>(((codepoint >> 12) & 0x3f) | 0x80);
        utf8buffer[2] = static_cast<char>(((codepoint >> 6) & 0x3f) | 0x80);
        utf8buffer[3] = static_cast<char>((codepoint & 0x3f) | 0x80);
        utf8buffer[4] = '\0';
        return true;
    } else {
        utf8buffer[0] = '\0';
        return false;
    }
}

int main(int argc, char *argv[])
{
    std::cerr << "This software is released under the so-called '3-Clause BSD License'" << std::endl;
    std::cerr << "Copyright 2019 Thomas Fischer <fischer@unix-ag.uni-kl.de>" << std::endl;

    /// Initialize ICU Transliterator
    UErrorCode uec = U_ZERO_ERROR;
    icu::Transliterator *translit = icu::Transliterator::createInstance("Any-Latin;Latin-ASCII", UTRANS_FORWARD, uec);
    if (U_FAILURE(uec)) {
        std::cerr << "Error creating an ICU Transliterator instance: " << u_errorName(uec) << std::endl;
        if (translit != nullptr) delete translit;
        return 1;
    }

    /// Mapping ASCII strings to each a list of Unicode code points (0x0 to 0xffff)
    /// which have this ASCII string as transliterated representation
    /// Example:  "a" -> [0x0061, 0x00e0, 0x00e0, 0x00e1, 0x00e2, 0x00e3, 0x00e4, ...]
    /// Custom compare function takes care then when iterating over map
    /// keys will be ordered by longest first, then lexicographically.
    std::map<std::string, std::vector<uint32_t>, first_len_then_lex> mapping;

    /// Assume that an plain ASCII representation is not longer than 256 bytes
    static const size_t buffer_size = 256;
    char buffer[buffer_size];
    /// Go over the first 65536 may Unicode code points
    for (uint32_t c = 0x0; c <= 0xffff; ++c) {
        if (c < 32 || c == 127) {
            /// ASCII control characters excluded
            buffer[0] = '\0';
        } else if (c >= 0xd800 && c <= 0xdfff) {
            /// Range for surrogates excluded
            buffer[0] = '\0';
        } else if (c < 127) {
            /// Normal ASCII characters represent themselves
            buffer[0] = static_cast<char>(c & 0x7f);
            buffer[1] = '\0';
        } else if (c <= 0xffff) {
            /// For Unicode code points starting from 128 (0x0080) ...
            /// Transliterate and write resulting string into buffer
            const char16_t u = static_cast<char16_t>(c & 0xffff);
            icu::UnicodeString uString = icu::UnicodeString(&u, 1);
            translit->transliterate(uString);
            const char16_t *ptr = uString.getTerminatedBuffer();
            size_t len = 0;
            for (; len < buffer_size && *ptr >= 32 && *ptr < 128; ++len, ++ptr)
                buffer[len] = (char)(*ptr & 0xff);
            buffer[len] = '\0';
        } else
            buffer[0] = '\0';

        if (buffer[0] == '\0') continue;

        /// Create C++ string from char*
        const std::string str(buffer);
        /// Insert string into mapping and record Unicode code point for which
        /// this string was generated
        /// Due to the nature of the map, every string is included only once, to
        /// allow for multiple Unicode code points to have the same ASCII representation,
        /// a vector records for each string its original Unicode code points.
        if (mapping.find(str) == mapping.end()) {
            std::vector<uint32_t> v = {c};
            mapping.insert({str, v});
        } else {
            std::vector<uint32_t> &v = mapping.at(str);
            v.push_back(c);
            mapping.insert({str, v});
        }

        if (c == 0xffff) break; ///< char16_t won't detect overflow and for loop would run forever
    }

    std::string unidecode_text;
    std::vector<size_t> unidecode_pos(65536);
    /// Go over mapping. The map's compare function takes care that long
    /// ASCII representation get processed first
    for (auto it = mapping.cbegin(); it != mapping.cend(); ++it) {
        const size_t len = it->first.length();
        size_t pos_in_unidecode_text = 0;
        if (len > 0) {
            pos_in_unidecode_text = unidecode_text.find(it->first);
            if (pos_in_unidecode_text == unidecode_text.npos) {
                pos_in_unidecode_text = unidecode_text.length();
                unidecode_text.append(it->first);
            }
        }
        /// Assuming that len <= 31, otherwise things will start breaking
        const size_t combined_value = (len & 31) | pos_in_unidecode_text << 5;
        for (const uint32_t c : it->second)
            unidecode_pos[c] = combined_value;
    }

    std::cout << "/// The following array contains only 65536-2048 entries" << std::endl;
    std::cout << "/// as it skips the range reserved for surrogates (U+D800 to U+DFFF)." << std::endl;
    std::cout << "/// To get the index for a code point c in the range 0x0 to 0xffff," << std::endl;
    std::cout << "/// apply the following formula:" << std::endl;
    std::cout << "///   index = c < 0xd800 ? c : c - 2048;" << std::endl;
    std::cout << "static const size_t[] unidecode_pos = {";
    for (uint32_t c = 0x0; c <= 0xffff; ++c) {
        if (c >= 0xd800 && c <= 0xdfff)
            continue;
        else if (c % 32 == 0) {
            /// Add line breaks every now and then
            if (c > 0)
                std::cout << ",";
            std::cout << std::endl << "    ";
        } else
            std::cout << ", ";
        /// Print combined position and length for a Unicode code point
        std::cout << unidecode_pos.at(c);
    }
    std::cout << std::endl << "};" << std::endl;

    std::cout << "static const char *unidecode_text =";
    /// Print text, but wrap it every 80 characters
    for (size_t i = 0; i < unidecode_text.length(); i += 80) {
        const std::string fragment = unidecode_text.substr(i, 80);
        std::cout << std::endl << "    \"";
        for (auto it = fragment.cbegin(); it != fragment.cend(); ++it) {
            if ((*it == '\\' && *(it + 1) != 'x') || *it == '"')
                std::cout << "\\";
            std::cout << *it;
        }
        std::cout << "\"";
    }
    std::cout << ";" << std::endl;

    delete translit;

    return 0;
}
